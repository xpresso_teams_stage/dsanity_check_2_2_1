""" Contains helper method for  users"""
import os
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import ControllerClientResponseException
from xpresso.ai.core.commons.utils import error_codes
from xpresso.ai.core.commons.utils import constants
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.utils.generic_utils import str_hash256

logger = XprLogger()


def user_info_check(user_json):
    """
    user_info_check checks if the userdata provided is sufficient enough
    """
    # These are mandatory fields that needs to be provided in userjson
    required_info = [
        'uid', 'pwd', 'firstName', 'lastName',
        'email', 'primaryRole'
    ]
    # primaryRole of a user has to one of these
    access_levels = ['Dev', 'PM', 'DH', 'Su', 'Admin']

    # checks if the mandatory fields are provided or not
    for val in required_info:
        if val not in user_json:
            return -1
        elif not len(user_json[val]):
            return -1

    # checks if the primaryRole is available or not
    if user_json['primaryRole'] not in access_levels:
        return 0

    return 1


def modify_user_check(changes_json):
    if 'uid' not in changes_json:
        return error_codes.incomplete_user_information
    # checks if the user password is also present in changes_json
    if 'primaryRole' in changes_json:
        roles = ['Dev', 'DH', 'PM', 'Su', 'Admin']
        if changes_json['primaryRole'] not in roles:
            return error_codes.incorrect_primaryRole
    elif 'pwd' in changes_json:
        return error_codes.cannot_modify_password
    elif 'activationStatus' in changes_json and \
            not changes_json['activationStatus']:
        return error_codes.call_deactivate_user

    return 200


def filter_user_output(users):
    filtered_users = []
    output_fields = [
        'uid', 'firstName', 'lastName',
        'email', 'primaryRole', 'nodes',
        'activationStatus'
    ]
    for user in users:
        new_user = {}
        for field in output_fields:
            if field in user:
                new_user[field] = user[field]
        filtered_users.append(new_user)
    return filtered_users


def save_token(token_file, token):
    """Token is saved in the local file system for """
    file = open(token_file, 'w+')
    file.write(token)
    file.close()
    logger.info('CLIENT : Token written to file. Exiting.')


def get_token(token_file):
    """Token is saved in the local file system for """
    token = None
    current_user = os.getenv('CURRENT_USER', '')
    current_user_enc = str_hash256(current_user)
    if current_user and current_user_enc and current_user_enc not in token_file:
        parts = token_file.split('/')
        parts.insert(-1, current_user_enc)
        token_file = '/'.join(parts)
    try:
        with open(token_file, "r") as f:
            token = f.read()
            token = token.strip("\n")
    except FileNotFoundError:
        logger.error("No Token Found. Need to Relogin")
        raise ControllerClientResponseException(
            "No session found. Please login.", error_codes.expired_token)
    return token


def get_token_file_path(base_path):

    current_user = os.getenv('CURRENT_USER', '')

    if current_user:
        current_user_enc = str_hash256(current_user)
        return os.path.join(base_path, current_user_enc, '.current')
    else:
        return os.path.join(base_path, '.current')


def get_workspace_file_path(base_path):
    current_user = os.getenv('CURRENT_USER', '')
    if current_user:
        current_user_enc = str_hash256(current_user)
        workspace_path = os.path.join(base_path, current_user_enc, '.workspace')
    else:
        workspace_path = os.path.join(base_path, '.workspace')

    if not os.path.exists(workspace_path):
        logger.debug("Workspace path does not exist. Creating a new one"
                     "from default config environment")
        config = XprConfigParser()
        predicted_ws = "qa"
        for ws, config_path in constants.config_paths.items():
            if config.config_file_path.endswith(config_path):
                predicted_ws = ws
                break
        parent_dir = os.path.dirname(workspace_path)
        try:
            if not os.path.exists(parent_dir):
                os.makedirs(name=parent_dir)
            with open(workspace_path, 'w+') as file:
                file.write(predicted_ws)
        except FileNotFoundError:
            logger.warning("Can not create workspace file")
        logger.debug("Written predicted workspace {} in {}"
                     .format(predicted_ws, workspace_path))
    return workspace_path


